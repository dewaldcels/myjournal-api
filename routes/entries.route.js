const { Router } = require('express');
const router      = Router();
const AppResponse = require('../models/response.model');
const Entry       = require('../models/entries.model');

const retriever = sequelize => ({
    getAll: async (req, res) => {
        const response = new AppResponse();
        const entry = new Entry(sequelize);

        try {
            response.data = await entry.getAll();
        }
        catch (e) {
            response.addError(500, e.toString());
        }

        return res.status(response.status).json(response);
    },
    getById: async (req, res) => {
        const response = new AppResponse();
        const entryModel = new Entry(sequelize);
        const { id } = req.params;

        try {
            if (!id) {
                throw ({ status: 400, message: 'No information about the entry received' });
            }
            response.data = await entryModel.getById(id);
        } catch (e) {
            const { status = 500, message = 'An error occurred while fetching the entry' } = e;
            response.addError(status, message);
        }

        return res.status(response.status).json(response);
    },
    getByIdWithAuthor: async(req, res) => {
        const { id } = req.params;
        const response = new AppResponse();
        const entryModel = new Entry(sequelize);

        try {
            response.data = Object.assign({}, ...(await entryModel.getByIdWithAuthor(id)));
        } catch (e) {
            const { status = 500, message = 'An error occurred while adding the author.' } = e;
            response.addError(status, message);
        }

        return res.status(response.status).json(response);
    },
    getByIdWithTags: async (req, res) => {

        const apiRes = new AppResponse();
        const entryModel = new Entry(sequelize);
        const { id } = req.params;

        try {
            const entry = await entryModel.getById(id);
            const tags = await entryModel.getByIdWithTags(id);
            apiRes.data = {
                entry: entry[0] || null,
                tags: [...tags]
            }
        } catch (e) {

        }

        return res.status(apiRes.status).json(apiRes);
    }
});

const creator = sequelize => ({
    create:  async (req, res) => {
        const apiResp = new AppResponse();
        const entryModel = new Entry(sequelize);
        const { entry } = await req.body;

        try {
            const [ insertId ] = await entryModel.create(entry);
            apiResp.data = {
                journal_entry_id: insertId,
                ...entry
            };
        } catch (e) {
            apiResp.handleError(e);
        }

        return res.status(apiResp.status).json(apiResp);
    }
})

const entryRoutes = router => sequelize => {

    router.get('/', retriever(sequelize).getAll);
    router.get('/:id', retriever(sequelize).getById);
    router.get('/:id/author', retriever(sequelize).getByIdWithAuthor);
    router.get('/:id/tags', retriever(sequelize).getByIdWithTags);
    router.post('/', creator(sequelize).create);

    return router;
};

module.exports = entryRoutes(Router());