const { Router } = require('express');
const AppResponse = require('../models/response.model');
const EntryTag = require('../models/entry-tag')

const creator = sequelize => ({
    create: async (req, res) => {
        console.log(req.body);
        const { journal_entry_id, tags } = req.body;
        const apiResp = new AppResponse();
        const entryTagModel = new EntryTag(sequelize);

        try { 
            const [ insertId, affectedRows ] = await entryTagModel.create(journal_entry_id, tags);
            if (insertId && affectedRows != tags.length) {
                throw ({status: 500, message: 'Could not link tags to journal entry.'});
            }
            
            apiResp.data = {
                success: true
            };
        }
        catch (e) {
            apiResp.addError(500, e.toString());
        }

        return res.status(apiResp.status).json(apiResp);
    }
})

const entryTagsRoutes = router => sequelize => {

    router.post('/', creator(sequelize).create);

    return router;
}

module.exports = entryTagsRoutes(Router());