const { Router } = require('express');
const AppResponse = require('../models/response.model');
const Tag = require('../models/tag.model');

const retriever = sequelize => ({
    getAll: async (req, res) => {
        const apiRes = new AppResponse();
        const tagModel = new Tag(sequelize);

        try {
            apiRes.data = await tagModel.getAll();
        } catch (e) {
            apiRes.addError(500, e);
        }
        return res.status(apiRes.status).json(apiRes);
    },
    getById: async (req, res) => {
        const apiRes = new AppResponse();
        const tagModel = new Tag(sequelize);
        const { id } = req.params;

        try {
            apiRes.data = await tagModel.getById(id);
        } catch (e) {
            apiRes.addError(500, e);
        }
        return res.status(apiRes.status).json(apiRes);
    }
})

const tagRoutes = router => sequelize => {

    router.get('/', retriever(sequelize).getAll);
    router.get('/:id', retriever(sequelize).getById);


    return router;
}

module.exports = tagRoutes(Router());