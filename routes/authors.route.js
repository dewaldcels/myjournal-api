const { QueryTypes } = require('sequelize');
const { Router } = require('express');
const AppResponse = require('../models/response.model');
const Author = require('../models/author.model');

const retriever = sequelize => ({
    getAll: async (req, res) => {
        const response = new AppResponse();
        const author = new Author(sequelize);

        try {
            response.data = await author.getAll();
        }
        catch (e) {
            response.addError(500, e.toString());
        }

        return res.status(response.status).json(response);
    },
    getById: async (req, res) => {
        const apiResp = new AppResponse();
        const author = new Author(sequelize);

        const { id = false } = req.params;

        try {

            if (!id) {
                throw ({ status: 400, message: 'No or invalid information for author was provided' });
            }

            apiResp.data = await author.getById(id);
        }
        catch (e) {
            apiResp.handleError(e);
        }

        return res.status(apiResp.status).json(apiResp);
    },
    getByIdWithEntries: async (req, res) => {
        const apiResp = new AppResponse();
        const authorModel = new Author(sequelize);
        const { id } = req.params;

        try {
            const author = await authorModel.getById(id);
            const entries = await authorModel.getByIdWithEntries(id);
            apiResp.data = {
                author: author[0] || null,
                entries: entries || []
            }
        } catch (e) {
            apiResp.handleError(e);
        }

        return res.status(apiResp.status).json(apiResp);
    }
});

const posts = (sequelize) => ({
    create: async (req, res) => {
        const response = new AppResponse();
        const authorModel = new Author(sequelize);
        const { author } = req.body;

        try {
            if (!authorModel.hasRequiredProps(author, ['first_name', 'last_name', 'email'])) {
                throw ({ status: 400, message: 'Received invalid author inforamtion.' });
            }

            const [insertId, affectedRows] = await authorModel.create(author);

            if (!insertId || affectedRows == 0) {
                throw ({ status: 500, message: 'Could not insert the author' });
            }

            response.data = await authorModel.getById(insertId);
            response.status = 201;

        }
        catch (e) {
            apiResp.handleError(e);
        }

        return res.status(response.status).json(response);
    }
});

const authorRoutes = router => sequelize => {

    router.get('/', retriever(sequelize).getAll);
    router.get('/:id', retriever(sequelize).getById);
    router.get('/:id/entries', retriever(sequelize).getByIdWithEntries);
    router.post('/', posts(sequelize).create);

    return router;
};

module.exports = authorRoutes(Router());