const { Router } = require('express');
const AppResponse = require('../models/response.model');
const Journal = require('../models/journal.model');

const retriever = sequelize => ({
    getAll: async (req, res) => {

        const response = new AppResponse();
        const journal = new Journal(sequelize);

        try {
            response.data = await journal.getAll();
        }
        catch (e) {
            response.addError(500, e.toString());
        }

        res.status(response.status).json(response);
    },
    getById: async (req, res) => {
        const { id } = req.params;
        const response = new AppResponse();
        const journal = new Journal(sequelize);

        try {
            response.data = await journal.getById(id);
        }
        catch (e) {
            response.addError(500, e.toString());
            console.error(e.toString());
        }

        res.status(response.status).json(response);
    },
    getByIdWithEntries: async (req, res) => {
        const apiResp = new AppResponse();
        const journalModel = new Journal(sequelize);
        const { id } = req.params;

        try { 

            const journal = await journalModel.getById(id);
            const entries = await journalModel.getByIdWithEntries(id);

            apiResp.data = {
                journal: journal[0] || null,
                entries
            };

        } catch (e) {
            const { status = 500, message = 'An unknown error occurred' } = e;
            apiResp.addError(status, message);
        }

        return res.status(apiResp.status).json(apiResp);
    }
});

const creator = sequelize => ({
    create: async (req, res) => {
        const response = new AppResponse();
        const { journal } = req.body;
        const journalModel = new Journal(sequelize);

        try {

            if (!journal || !journal.name) {
                throw ({ status: 400, message: 'Journal data received is incompleted' });
            }

            const [insertedId = -1, affectedRows] = await journalModel.create(journal);

            if (insertedId < 0) {
                throw ({ status: 500, message: 'Could not create new journal' });
            }

            response.data = await journalModel.getById(insertedId);
            response.status = 201;

        }
        catch (e) {
            const { status = 500, message = 'An error occured while adding Journal' } = e;
            response.addError(status, message);
        }

        return res.status(response.status).json(response);
    }
});

const updator = sequelize => ({
    update: async (req, res) => {

        const { journal } = req.body;
        const jrnl = new Journal(sequelize);

        try {
            response.data = await jrnl.update(journal);
        }
        catch (e) {
            response.addError(500, e.toString());
        }

        return res.status(response.status).json(response);
    }
});

const deletor = sequelize => ({
    delete: async (req, res) => {
        const response = new AppResponse();
        const journalModel = new Journal(sequelize);
        const { id = false } = req.params;
        try {

            if (id === false) {
                throw ({ status: 400, message: 'Please provide the journal the must be deleted' });
            }

            await journalModel.delete(id);

            response.data = {
                result: 'Successfully deleted Journal'
            }

        } catch (e) {
            response.addError(500, e.toString());
        }

        return res.status(response.status).json(response);
    }
});

const journalRoutes = router => sequelize => {

    router.get('/', retriever(sequelize).getAll);
    router.get('/:id', retriever(sequelize).getById);
    router.get('/:id/entries', retriever(sequelize).getByIdWithEntries)
    router.post('/', creator(sequelize).create);
    router.patch('/:id', updator(sequelize).update);
    router.delete(':/id', deletor(sequelize).delete);

    return router;
};

module.exports = journalRoutes(Router());