class EntryTag {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }

    create(journalEntryId, tags) {
        const values = tags.map(tag => `(?)`);
        const replacements = tags.map(tagId => [tagId, journalEntryId]);
        return this.sequelize.query(`
            INSERT INTO journal_entry_tag ( tag_id, journal_entry_id ) VALUES ${values.join(',')};
        `, {
            type: this.sequelize.QueryTypes.INSERT,
            replacements: replacements
        });
    }
}

module.exports = EntryTag;