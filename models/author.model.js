const Model = require('./model');

class Author extends Model {

    constructor(sequelize) {
        super();
        this.sequelize = sequelize;
    }

    getAll() {
        return this.sequelize.query('SELECT * FROM author', {
            type: this.sequelize.QueryTypes.SELECT
        });
    }

    getById(id) {
        return this.sequelize.query('SELECT * FROM author WHERE author_id = :author_id', {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                author_id: id
            }
        });
    }

    getByIdWithEntries(id) {
        return this.sequelize.query(`
            SELECT 
                journal_entry_id AS 'journal_entry.journal_entry_id',
                title AS 'journal_entry.title',
                content AS 'journal_entry.content',
                journal_entry.created_at AS 'journal_entry.created_at',
                journal_entry.updated_at AS 'journal_entry.updated_at',
                journal_entry.journal_id AS 'journal.journal_id',
                journal.name AS 'journal.name'
            FROM journal_entry
            JOIN journal 
                ON journal.journal_id = journal_entry.journal_id
            WHERE journal_entry.author_id = :author_id
        `, {
            nest: true,
            type: this.sequelize.QueryTypes.SELECT,
            replacements: { author_id: id }
        });
    }

    create(author) {
        return this.sequelize.query('INSERT INTO author (first_name, last_name, email) VALUES (:first_name, :last_name, :email)', {
            type: this.sequelize.QueryTypes.INSERT,
            replacements: {
                first_name: author.first_name,
                last_name: author.last_name,
                email: author.email
            }
        });
    }
}

module.exports = Author;