class Model {
    
    constructor() {
    }

    hasRequiredProps(data, cols) {
        const result =  cols.reduce((prev, curr) => {
            return [...prev, data.hasOwnProperty(curr)];
        }, []);
        return result.includes(false) ? false : true;
    }
}

module.exports = Model;