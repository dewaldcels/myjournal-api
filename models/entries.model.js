class Entry {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }

    getAll() {
        return this.sequelize.query('SELECT * FROM journal_entry', {
            type: this.sequelize.QueryTypes.SELECT
        });
    }

    getById(id, cols = '*') {
        return this.sequelize.query(`SELECT ${cols} FROM journal_entry WHERE journal_entry_id = :journal_entry_id`, {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                journal_entry_id: id
            }
        });
    }

    getByIdWithAuthor(id) {
        return this.sequelize.query(
            `SELECT 
                first_name AS 'author.first_name',
                last_name AS 'author.last_name',
                email AS 'author.email',
                je.journal_entry_id AS 'journal_entry.journal_entry_id',
                je.title AS 'journal_entry.title',
                je.content AS 'journal_entry.content',
                je.created_at AS 'journal_entry.created_at',
                je.updated_at AS 'journal_entry.updated_at'
            FROM journal_entry AS je
                INNER JOIN author USING (author_id)
            WHERE journal_entry_id = :journal_entry_id`, {
            nest: true,
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                journal_entry_id: id
            }
        })
    }

    getByIdWithTags(id) {

        return this.sequelize.query(`
                SELECT 
                    tag.tag_id,
                    tag.name
                FROM journal_entry_tag
                JOIN tag ON tag.tag_id = journal_entry_tag.tag_id
                WHERE journal_entry_tag.journal_entry_id = :journal_entry_id
            `, {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                journal_entry_id: id
            }
        })

    }

    create(entry) {
        return this.sequelize.query(`
            INSERT INTO journal_entry ( title, content, author_id, journal_id )
            VALUES 
            (:title, :content, :author_id, :journal_id)
        `, {
            type: this.sequelize.QueryTypes.INSERT,
            replacements: {
                title: entry.title,
                content: entry.content,
                author_id: entry.author_id,
                journal_id: entry.journal_id
            }
        })
    }
}

module.exports = Entry;