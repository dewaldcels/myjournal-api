class AppResponse {
    constructor(status = 200, data = []) {
        this.status = status;
        this.data = data;
        this.error = null;
    }

    addError(status, error) {
        this.error = error;
        this.status = status;
    }

    handleError(error) {
        const { status = 500, message = 'Unknown error occurred' } = error;
        this.status = status;
        this.error = message;
    }
}

module.exports = AppResponse;