class Tag {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }

    getAll() {
        return this.sequelize.query('SELECT * FROM tag', {
            type: this.sequelize.QueryTypes.SELECT
        });
    }

    getById(id) {
        return this.sequelize.query('SELECT * FROM tag WHERE tag_id = :tag_id', {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                tag_id: id
            }
        });
    }
}


module.exports = Tag;