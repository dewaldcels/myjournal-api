class Journal {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }

    getAll() {
        return this.sequelize.query('SELECT * FROM journal', {
            type: this.sequelize.QueryTypes.SELECT
        });
    }

    getById(id) {
        return this.sequelize.query('SELECT * FROM journal WHERE journal_id = :journal_id', {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                journal_id: id
            }
        });
    }

    getByIdWithEntries(id) {
        return this.sequelize.query(`
            SELECT
                journal_entry.journal_entry_id,
                journal_entry.title,
                journal_entry.content,
                journal_entry.created_at,
                journal_entry.updated_at
            FROM journal_entry
            WHERE journal_entry.journal_id = :journal_id
        `, {
            type: this.sequelize.QueryTypes.SELECT,
            replacements: {
                journal_id: id
            }
        });
    }

    create(journal) {
        return this.sequelize.query('INSERT INTO journal (name) VALUES (:name)', {
            type: this.sequelize.QueryTypes.INSERT,
            replacements: {
                name: journal.name
            }
        });
    }

    update(journal) {
        return this.sequelize.query('UPDATE journal SET name = :name WHERE journal_id = :journal_id', {
            type: this.sequelize.QueryTypes.UPDATE,
            replacements: {
                name: journal.name,
                journal_id: journal.journal_id
            }
        });
    }

    delete(journal_id) {
        return this.sequelize.query('DELETE FROM journal WHERE journal_id = :journal_id', {
            type: this.sequelize.QueryTypes.DELETE,
            replacements: {
                journal_id: journal_id
            }
        });
    }
}

module.exports = Journal;