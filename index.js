require('dotenv').config();

const cors = require('cors');
const express = require('express');
const { PORT = 3000 } = process.env;
const { Sequelize } = require('sequelize');

const app = express();

// Express Middleware for JSON responses
app.use(express.json());
// Middleware to support CORS
app.use(cors());
// Express Middleware to add the correct API header.
app.use((req, res, next) => {
    console.log('Request: ', req.path);
    console.log('Time:', new Date().toUTCString('en'));
    res.header('Content-Type', 'application/json');
    next();
});

// Initialize Database
const sequelize = new Sequelize({
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    dialect: process.env.DB_DIALECT
});

/**
 * Test if we can make a connection to the database.
 */
sequelize.authenticate({ raw: true })
    .then(() => { // Successfully connected to database.
        
        // Create Routes
        app.use('/v1/api/journals',   require('./routes/journals.route')(sequelize));
        app.use('/v1/api/entries',    require('./routes/entries.route')(sequelize));
        app.use('/v1/api/authors',    require('./routes/authors.route')(sequelize));
        app.use('/v1/api/tags',       require('./routes/tags.route')(sequelize));
        app.use('/v1/api/entry-tags', require('./routes/entry-tags.route')(sequelize));

        // Start App Server
        app.listen(PORT, () => console.log(`App started on port ${PORT}...`));
    })
    .catch(e => { // Could not connect to database.
        console.error(e.toString());
    });

