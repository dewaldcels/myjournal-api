# MyJournal API

NodeJS API for the MyJournal website. Connects to a MySQL.

## Setup
Create a local .env file with the following fields:

```
DB_HOST=localhost
DB_DATABASE=your_db_name
DB_USER=your_db_user
DB_PASS=your_db_password
DB_DIALECT=mysql
```

## Database
MySQL files available on Request for Table creation.


## Install
To install the dependencies for the project you must run the following in the root directory of the project.

```bash
npm install 
```

## Run
To run the application you must execute the following command in the terminal in the root directory of the project.

```bash
npm start 
```